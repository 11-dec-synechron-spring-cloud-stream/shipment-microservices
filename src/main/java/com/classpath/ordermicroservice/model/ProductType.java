package com.classpath.ordermicroservice.model;

public enum ProductType {
    APPLE,
    SAMSUNG,
    OPPO,
    VIVO
}