package com.classpath.ordermicroservice.processor;

import com.classpath.ordermicroservice.channel.OutputChannels;
import com.classpath.ordermicroservice.model.Product;
import com.classpath.ordermicroservice.model.Shipment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class OrderProcessor {

    private final OutputChannels outputChannels;

    @StreamListener("orderInputChannel")
    public void processOrder(Product product) {

        log.info("Processing the order :: {} ", product);
        Shipment shipment = Shipment.builder().city("Bangalore").status(true).zipCode("577142").build();
        //publish the message to output channel
        this.outputChannels.shipmentOutputChannel().send(MessageBuilder.withPayload(shipment).build());
    }
}