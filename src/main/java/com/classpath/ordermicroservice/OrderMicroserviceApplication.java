package com.classpath.ordermicroservice;

import com.classpath.ordermicroservice.channel.InputChannels;
import com.classpath.ordermicroservice.channel.OutputChannels;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;

@SpringBootApplication
@EnableBinding({InputChannels.class, OutputChannels.class})
public class OrderMicroserviceApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderMicroserviceApplication.class, args);
    }
}
