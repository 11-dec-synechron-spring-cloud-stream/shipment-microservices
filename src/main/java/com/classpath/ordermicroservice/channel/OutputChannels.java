package com.classpath.ordermicroservice.channel;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface OutputChannels {

    @Output("shipmentOutputChannel")
    MessageChannel shipmentOutputChannel();
}