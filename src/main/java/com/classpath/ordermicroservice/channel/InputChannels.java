package com.classpath.ordermicroservice.channel;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface InputChannels {

    @Input("orderInputChannel")
    SubscribableChannel orderInputChannel();
}